class CreateExchanges < ActiveRecord::Migration[6.1]
  def change
    create_table :exchanges do |t|
      t.float :rate
      t.datetime :validate_date
      t.float :percent_sell, :default => 3
      t.float :percent_buy, :default => 1

      t.timestamps
    end
  end
end
