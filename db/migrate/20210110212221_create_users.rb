class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :password_digest
      t.float :balance_USD
      t.float :balance_BTC

      t.timestamps
    end
    add_index :users, :email, unique: true
  end
end
