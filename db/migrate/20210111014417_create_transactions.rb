class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.float :amount_send
      t.float :amount_received 
      t.string :currency_send
      t.string :currency_receiver
      t.float :rate_exchange
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
