# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create([
    {name: 'David', surname: 'Barrios', email: 'barriosd1921@gmail.com', password: '12345'}
])

# exchange = Exchange.create([
#     {rate: 34898.9955, validate_date: "2021-01-12T20:19:40.550Z",percent_sell: 3, percent_buy: 1}
# ])