require 'rails_helper'

describe 'Transaction Controller - API', type: :request do
    include Docs::Transaction::Api
    it 'GET /api/v1/transaction/getTransactionsByUser' do
        include Docs::Transaction::Gettransactionsbyuser
        user = FactoryBot.create(:user)
        
        get '/api/v1/transaction/getTransactionsByUser', as: :json, headers: {:Authorization => "Bearer "+user.token}

        expect(response).to have_http_status(:ok).or(:bad_request)
    end

    it 'GET /api/v1/transaction/getTransactionById/:id' do
        include Docs::Transaction::Gettransactionbyid
        user = FactoryBot.create(:user)
        transaction = FactoryBot.create(:transaction, amount_send: "0.001", currency_send: "BTC", currency_receiver: "USD", user: user)
        transaction = FactoryBot.create(:transaction, amount_send: "1", currency_send: "USD", currency_receiver: "BTC", user: user)

        get '/api/v1/transaction/getTransactionById/1', as: :json, headers: {:Authorization => "Bearer "+user.token}


        expect(response).to have_http_status(:ok).or(:no_content)
    end

    it 'PUT /api/v1/transaction/setTransaction' do
        include Docs::Transaction::Settransaction
        user = FactoryBot.create(:user)

        put '/api/v1/transaction/setTransaction', params: {"data" => "{\"amount_send\": \"0,0001\",\"currency_send\": \"BTC\",\"currency_receiver\":\"USD\"}"}, headers: {:Authorization => "Bearer "+user.token}


        expect(response).to have_http_status(:ok)
    end
end