require 'rails_helper'

describe 'User Controller - API', type: :request do
    include Docs::User::Api
    it 'POST /api/v1/user/register', :dox do
        include Docs::User::Login
        post '/api/v1/user/register', params: {"user"=>"{\"name\": \"Lucia\",\"surname\": \"Barrios\",\"email\":\"test@test.com\",\"password\":\"12345\",\"repassword\":\"12345\"}"}

        expect(response).to have_http_status(:created)
    end

    it 'POST /api/v1/user/login', :dox do
        include Docs::User::Register
        post '/api/v1/user/login', params: {"user"=>"{\"email\":\"barriosd1921@gmail.com\",\"password\":\"12345\"}"}

        expect(response).to have_http_status(:ok)
    end
end