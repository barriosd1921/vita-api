require 'rails_helper'

describe 'Exchange Controller - API', type: :request do
    include Docs::Exchange::Api
    it 'GET /api/v1/Exchange/getExchange' do
        include Docs::Exchange::Getexchange
        user = FactoryBot.create(:user)
        
        get '/api/v1/exchange/getExchange', as: :json, headers: {:Authorization => "Bearer "+user.token}
        expect(response).to have_http_status(:ok).or(:created)
    end
end