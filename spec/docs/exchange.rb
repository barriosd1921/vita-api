module Docs
    module Exchange
      extend Dox::DSL::Syntax
      document :api do
        resource "Exchange" do
          endpoint "/api/v1/exchange"
          group "Exchange"
          desc "exchange.md"
        end
      end
      
      document :getexchange do
        action "Get current exchange"
      end
    end
  end