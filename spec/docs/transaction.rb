module Docs
    module Transaction
      extend Dox::DSL::Syntax
      document :api do
        resource "Transaction" do
          endpoint "/api/v1/transaction"
          group "Transaction"
          desc "transaction.md"
        end
      end
      
      document :settransaction do
        action "Create new transaction by user"
      end

      document :gettransactionsbyuser do
        action "Get all transactions by user"
      end

      document :gettransactionbyid do
        action "Get details transaction by id and user"
      end
    end
  end

  

