module Docs
    module User
      extend Dox::DSL::Syntax
      document :api do
        resource "User" do
          endpoint "/api/v1/user"
          group "User"
          desc "user.md"
        end
      end
      
      document :login do
        action "Login user"
      end
      
      document :register do
        action "Create user"
      end
    end
  end