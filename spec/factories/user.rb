FactoryBot.define do
    factory :user do
        name { Faker::Name.name }
        surname  { Faker::Name.last_name }
        email { Faker::Internet.email }
        password { Faker::Internet.password }
        balance_USD { 1000 }
        balance_BTC { 0.1 }
    end
end