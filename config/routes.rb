Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  # Rutas para usuario
  post "/api/v1/user/login", to: "user#login"
  post "/api/v1/user/register", to: "user#register"
  get "/api/v1/user/updateSession", to: "user#updateSession"
  # get "/api/v1/user/getUsers", to: "user#getUsers"


  # Rutas para Exchange 
  get "/api/v1/exchange/getExchange", to: "exchange#getExchange"

  # Rutas para transacciones
  put "/api/v1/transaction/setTransaction", to: "transaction#setTransaction"
  get "/api/v1/transaction/getTransactionsByUser", to: "transaction#getTransactionsByUser"
  get "/api/v1/transaction/getTransactionById/:id", to: "transaction#getTransactionById"
end
