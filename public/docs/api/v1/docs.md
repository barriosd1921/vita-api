API - VITA TEST DAVID BARRIOS

api generada como prueba de conocimientos en Ruby on Rails, para optar al puesto de desarrollor en vitawallet.io

# Group User


## User [/api/v1/user]
Usuario resource

aqui va esto

###  [POST /api/v1/user/register]


+ Request POST /api/v1/user/register
**POST**&nbsp;&nbsp;`/api/v1/user/register`

    + Headers

            Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5
            Content-Type: application/x-www-form-urlencoded

    + Body

            user=%7B%22name%22%3A+%22Lucia%22%2C%22surname%22%3A+%22Barrios%22%2C%22email%22%3A%22test%40test.com%22%2C%22password%22%3A%2212345%22%2C%22repassword%22%3A%2212345%22%7D

+ Response 201

    + Headers

            Content-Type: application/json; charset=utf-8

    + Body

            {
              "status": "success",
              "message": "Registro satisfactorio",
              "code": 201
            }

+ Request POST /api/v1/user/login
**POST**&nbsp;&nbsp;`/api/v1/user/login`

    + Headers

            Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5
            Content-Type: application/x-www-form-urlencoded

    + Body

            user=%7B%22email%22%3A%22barriosd1921%40gmail.com%22%2C%22password%22%3A%2212345%22%7D

+ Response 200

    + Headers

            Content-Type: application/json; charset=utf-8

    + Body

            {
              "status": "success",
              "message": "Login correcto",
              "code": 200,
              "user": {
                "id": 1,
                "name": "David",
                "surname": "Barrios",
                "email": "barriosd1921@gmail.com",
                "password_digest": "$2a$12$/4G43wrU2Rly0Mj9H73zkenDQgJj8hkaHdQAb/XATNu9twm.ssooe",
                "balance_USD": 1000.0,
                "balance_BTC": 0.1,
                "created_at": "2021-01-13T20:59:49.342Z",
                "updated_at": "2021-01-13T20:59:49.342Z",
                "token": "ef6459d2bf44bbcbfe85311e260d2a28",
                "role": "ROLE_USER"
              }
            }
