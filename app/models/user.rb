class User < ApplicationRecord
    has_many :transactions
    has_secure_password
    before_create -> {self.token = generate_token}
    before_create -> {self.balance_BTC = 0.1}
    before_create -> {self.balance_USD = 1000.00}
    before_create -> {self.role = 'ROLE_USER'}
    
    private
 
    def generate_token
        loop do
            token = SecureRandom.hex
            return token unless User.exists?({token: token})
        end
    end
end
