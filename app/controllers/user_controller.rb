class UserController < ActionController::API
    include ActionController::HttpAuthentication::Token::ControllerMethods
    before_action :authenticate, only: [:update, :updateSession]
    before_action :get_object, only: [:login, :register]

    def login  
        user = User.find_by( email: @user_request['email'] )
        if user && user.authenticate( @user_request['password'] )
            render json: {
                "status" => "success",
                "message" => "Login correcto",
                "code" => 200,
                "user" => user
            }, status: :ok
        else
            render json: {
                "status" => "error",
                "message" => "Credenciales incorrectas",
                "code" => 401 
            }, status: :unauthorized
        end
    end

    def updateSession
        render json: {
            "status" => "success",
            "code" => 200,
            "user" => @user
        }, status: :ok
    end

    def register
        user = User.new(@user_request.except!("repassword"))
        if user.save
            render json: {
                "status" => "success",
                "message" => "Registro satisfactorio",
                "code" => 201 
            }, status: :created
        else
            render json: {
                "status" => "error",
                "message" => "No se realizo registro de usuario",
                "code" => 400,
                "errors" => user.errors 
            }, status: :bad_request
        end
    end

    # def getUsers
    #     users = User.all
    #     if users
    #         render json: users, status: :ok
    #     else
    #         render json: users.errors, status: :bad_request
    # end

    private

    def authenticate
        authenticate_or_request_with_http_token do |token, options|
            @user = User.find_by(token: token)
        end
    end

    def get_object
        @user_request = ActiveSupport::JSON.decode( params[:user] )
    end
end