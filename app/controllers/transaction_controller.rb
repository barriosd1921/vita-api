class TransactionController < ActionController::API
    include ActionController::HttpAuthentication::Token::ControllerMethods
    before_action :authenticate
    before_action :getExchange, only: [:setTransaction]

    def setTransaction
        data = ActiveSupport::JSON.decode( params[:data] )
        data['amount_send'] = (data['amount_send'].gsub '.', '')
        data['amount_send'] = (data['amount_send'].gsub ',', '.').to_f

        if data['currency_send'] == 'USD' 
            if @user['balance_USD'] < data['amount_send']

                render json: {
                    "status" => "error",
                    "code" => 402,
                    "message" => "Fondos en USD insuficientes"
                }, status: :payment_required
            else
                transaction = @user.transactions.new({
                    "amount_send" => data['amount_send'].to_f,
                    "amount_received" => (data['amount_send']/rate_sell).to_d.truncate(8).to_f,
                    "currency_send" => data['currency_send'],
                    "currency_receiver" => data['currency_receiver'],
                    "rate_exchange" => rate_sell
                });

                if transaction.save

                    @user.update(
                        balance_USD: (@user['balance_USD'] - data['amount_send']).to_d.truncate(2).to_f, 
                        balance_BTC: (@user['balance_BTC'] + (data['amount_send']/rate_sell)).to_d.truncate(8).to_f
                    )

                    render json: {
                        "status" => "success",
                        "code" => 200,
                        "message" => "Transaccion realizada, Compraste "+(data['amount_send']/rate_sell).to_d.truncate(8).to_s+" BTC",
                    }, status: :ok
                else
                    render json: {
                        "status" => "error",
                        "code" => 400,
                        "message" => "Error inesperado al persistir la transaccion",
                        "erros" => transaction.errors
                    }, status: :bad_request
                end
            end
        else
            if @user['balance_BTC'] < data['amount_send']
                render json: {
                    "status" => "error",
                    "code" => 402,
                    "message" => "Fondos en BTC insuficientes"
                }, status: :payment_required
            else
                transaction = @user.transactions.new({
                    "amount_send" => data['amount_send'],
                    "amount_received" => (data['amount_send']*rate_buy).to_d.truncate(2).to_f,
                    "currency_send" => data['currency_send'],
                    "currency_receiver" => data['currency_receiver'],
                    "rate_exchange" => rate_buy
                });

                if transaction.save
                    @user.update(
                        balance_USD: (@user['balance_USD']+(data['amount_send']*rate_buy)).to_d.truncate(2).to_f, 
                        balance_BTC: (@user['balance_BTC'] - data['amount_send']).to_d.truncate(8).to_f
                    )

                    render json: {
                        "status" => "success",
                        "code" => 200,
                        "message" => "Transaccion realizada, Compraste "+(data['amount_send']*rate_buy).to_d.truncate(2).to_s+" USD",
                    }, status: :ok
                else
                    render json: {
                        "status" => "error",
                        "code" => 400,
                        "message" => "Error inesperado al persistir la transaccion",
                        "erros" => transaction.errors
                    }, status: :bad_request
                end
                
            end
        end
    end

    def getTransactionsByUser
        transactions = @user.transactions.all
        if transactions
            render json: {
                "status" => "success",
                "code" => 200,
                "Messege" => "Transacsiones realizadas por el usuario con email: "+@user[:email],
                "transactions" => transactions
            }, status: :ok
        else
            render json: {
                "status" => "error",
                "code" => 400,
                "Messege" => "Error al tratar de extraer solicitudes del usuario con email: "+@user[:email],
                "errors" => transactions.errors
            }, status: :bad_request
        end
    end

    def getTransactionById
        transaction = @user.transactions.find(params[:id])

        if transaction
            render json: {
                "status" => "success",
                "code" => 200,
                "messege" => "Transaccion con id "+params[:id].to_s,
                "transaction" => transaction
            }, status: :ok
        else
            render json: {
                "status" => "success",
                "code" => 204,
                "messege" => "Transaccion con id "+params[:id].to_s+" no existe"
            }, status: :no_content
        end
    end

    private

    def authenticate
        authenticate_or_request_with_http_token do |token, options|
            @user = User.find_by(token: token)
        end
    end

    def getExchange
        @exchange = Exchange.first
    end

    def rate_sell
        return @exchange[:rate]*(1+(@exchange[:percent_sell]/100))
    end

    def rate_buy
        return @exchange[:rate]*(1-(@exchange[:percent_buy]/100))
    end
end