require 'rest-client'


class ExchangeController < ActionController::API
    include ActionController::HttpAuthentication::Token::ControllerMethods
    before_action :authenticate

    def getExchange
        #tomo el primer o unico valor de la base de datos
        exchange = Exchange.first

        # si existe un valor ya cargado en base de datos 
        if exchange
            # valido si mi fecha actual es menor que mi fecha de validacion
            # de ser asi, envio el dato almacenado en la base de datos
            # en caso contrario vuelvo a llamar el endpoint de coindesk
            if exchange[:validate_date] > Time.zone.now               
                render json: {
                    "status" => "success",
                    "code" => 200,
                    "message" => "Exchange almacenado en cache",
                    "exchange" => exchange,
                }, status: :ok
            else
                exchange.update({
                    "rate" => get_exchange_rest['rate_float'],
                    "validate_date" => ( Time.zone.now + 180)
                });
                render json: {
                    "status" => "success",
                    "code" => 200,
                    "message" => "Exchange tomado de la nube (UPDATE)",
                    "exchange" => exchange,
                }, status: :ok
            end
        # Si no existe, llamo el metodo get_exchange_rest para solicitar un nuevo valor del endpoint coindesk
        # y le agrego un tiempo de validacion a la carga de 180 seg o 3 min
        else
            new_exchange = Exchange.create({
                "rate" => get_exchange_rest['rate_float'],
                "validate_date" => ( Time.zone.now + 180)
            });
            render json: {
                "status" => "success",
                "code" => 201,
                "message" => "Exchange tomado de la nube (CREATE)",
                "exchange" => new_exchange,
            }, status: :created
        end
        
    end

    private

    def authenticate
        authenticate_or_request_with_http_token do |token, options|
            @user = User.find_by(token: token)
        end
    end

    def get_exchange_rest
        response = RestClient.get("https://api.coindesk.com/v1/bpi/currentprice.json")
        return ActiveSupport::JSON.decode( response )['bpi']['USD']
    end
end